# -*- coding: utf-8 -*-
"""Setup the para application"""
from para.core import model
import transaction
from datetime import datetime


def bootstrap(command, conf, vars):
    """Place any commands to setup para here"""

    print 'Bootstrapping Data...',
    # <websetup.bootstrap.before.auth
    from sqlalchemy.exc import IntegrityError
    session = model.DBSession
    try:
        u = model.User()
        u.user_name = u'manager'
        u.email_address = u'manager@somedomain.com'
        u.password = u'managepass'
    
        session.add(u)
    
        g = model.Group()
        g.group_name = u'managers'
        g.display_name = u'Managers Group'
    
        g.users.append(u)
    
        session.add(g)
    
        p = model.Permission()
        p.permission_name = u'manage'
        p.description = u'This permission give an administrative right to the bearer'
        p.groups.append(g)
    
        session.add(p)
    
        u1 = model.User()
        u1.user_name = u'editor'
        u1.email_address = u'editor@somedomain.com'
        u1.password = u'editpass'

        session.add(u1)
        session.flush()

        statuses = [u'Draft', u'Pending', u'Published', u'Archived']

        for status in statuses:
            s = model.Status(name=status)
            session.add(s)

        languages = {u'English': u'en', u'Spanish': u'es'}

        for name, code in languages.iteritems():
            l = model.Language(name=name, code=code)
            session.add(l)

        session.flush()

        lorem_ipsum = u"""
        Lorem ipsum <font color="red">dolor</font> sit amet,
        consectetur adipisicing elit, sed do
        eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
        enim ad minim veniam, quis nostrud exercitation ullamco laboris
         nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
         in reprehenderit in voluptate velit esse cillum dolore eu
         fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
        """

        sed_ut = u"""
        Sed ut <b>perspiciatis</b> unde omnis iste natus error sit
        voluptatem accusantium doloremque laudantium, totam
        rem aperiam, eaque ipsa quae ab illo inventore
        veritatis et quasi architecto beatae vitae dicta
        sunt explicabo. Nemo enim ipsam voluptatem quia voluptas
        sit aspernatur aut odit aut fugit, sed quia consequuntur
        magni dolores eos qui ratione voluptatem sequi nesciunt. Neque
        porro quisquam est, qui dolorem ipsum quia dolor sit amet,
        consectetur, adipisci velit, sed quia non numquam eius modi
        tempora incidunt ut labore et dolore magnam aliquam quaerat
        voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem
        ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi
        consequatur? Quis autem vel eum iure reprehenderit qui in ea
        voluptate velit esse quam nihil molestiae consequatur, vel
        illum qui dolorem eum fugiat quo voluptas nulla pariatur?"""

        blogs = [{'title': u'Company Goals',
                  'status_id': 3, 'content':
u"""
<p>Hi! Chris Here.  I started Iterative Design because I wanted to create a
consulting firm that was better.  Better for the clients, better for the
developers.  I want to create a company that is more responsive, more adaptive.
</p>
<p>
I have personally been developing for over 15 years and have a stable of developers
who have similar levels of experience that are just chomping at the bit to create
your next project.
</p>
""",
                  'slug': u'company-goals',
                  'featured': True
                  }
                 ]
        #blogs.reverse()

        for blog in blogs:
            b = model.BlogEntry(**blog)
            b.published = datetime.now()
            b.authors.append(u1)
            b.last_modified_by = u1
            session.add(b)

        session.flush()

        settings = {u'fax': u'',
                    u'phone': u'303-638-3555',
                    u'address': u'10135 W. Dartmouth Pl.',
                    u'city': u'Lakewood',
                    u'state': u'CO',
                    u'country': u'US',
                    u'zip': u'80227',
                    u'email': u'info@iterative-design.com',
                    u'company_name': u'Iterative Design Ltd.',
                    }

        for key, value in settings.iteritems():
            s = model.Setting(name=key, value=value)
            session.add(s)

        session.flush()

        transaction.commit()
    except IntegrityError:
        print('Warning, there was a problem adding your auth data, it may have already been added:')
        import traceback
        print(traceback.format_exc())
        transaction.abort()
        print('Continuing with bootstrapping...')

    print 'done.'
    # <websetup.bootstrap.after.auth>
