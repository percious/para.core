from tgext.admin.controller import AdminController as TGAdminController
from tgext.admin.config import AdminConfig

from page import PageConfig
from user import UserConfig
from blog import BlogEntryConfig
from event import EventConfig
from setting import SettingConfig


class AdminConfig(AdminConfig):
    blogentry = BlogEntryConfig
    event = EventConfig
    page = PageConfig
    user = UserConfig
    setting = SettingConfig


class AdminController(TGAdminController):
    """
    The admin controller for the para application.
    """