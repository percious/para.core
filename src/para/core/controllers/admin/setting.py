# -*- coding: utf-8 -*-
"""Setting Admin Setup"""

from datetime import datetime
from slugify import slugify

from tg import redirect, expose, request

from tw2.ckeditor import CKEditor
from tw2.forms import TextField

from sprox.formbase import AddRecordForm, EditableForm
from sprox.fillerbase import TableFiller

from tgext.admin.config import CrudRestControllerConfig
from tgext.crud.controller import CrudRestController

from para.core.model.settings import Setting
from para.core.model.status import STATUS_PUBLISHED_ID


class SettingTableFiller(TableFiller):
    __model__ = Setting

class EditSettingForm(EditableForm):
    __model__ = Setting


class NewSettingForm(AddRecordForm):
    __model__ = Setting
    __add_fields__ = {'the_name': TextField('name')}


class SettingConfig(CrudRestControllerConfig):
    table_filler_type = SettingTableFiller
    new_form_type = NewSettingForm
    edit_form_type = EditSettingForm