# -*- coding: utf-8 -*-
"""Page Admin Setup"""

from datetime import datetime
from slugify import slugify

from tg import redirect, expose, request

from tw2.ckeditor import CKEditor
from tw2.forms import TextField

from sprox.formbase import AddRecordForm, EditableForm
from sprox.fillerbase import TableFiller

from tgext.admin.config import CrudRestControllerConfig
from tgext.crud.controller import CrudRestController

from para.core.model.page import Page, featured_pages
from para.core.model.status import STATUS_PUBLISHED_ID


class PageTableFiller(TableFiller):
    __model__ = Page

    def content(self, obj):
        return obj.content[:100] + '...'

    def meta_keywords(self, obj):
        if obj.meta_keywords and len(obj.meta_keywords)>100:
            return obj.meta_keywords[:100] + '...'
        return obj.meta_keywords

    def meta_description(self, obj):
        if obj.meta_description and len(obj.meta_description)>100:
            return obj.meta_description[:100] + '...'
        return obj.meta_description


class EditPageForm(EditableForm):
    __model__ = Page
    __omit_fields__ = ['children', 'last_modified', 'last_modified_by', 'authors', 'page_id']
    __field_order__ = ['title', 'abstract', 'content', 'language', 'parent', 'status', 'featured', 'created', 'published']
    __dropdown_field_names__ = ['title', 'user_name', 'name']
    content = CKEditor(id='content')
    title = TextField


class NewPageForm(AddRecordForm):
    __model__ = Page
    __omit_fields__ = ['children', 'last_modified', 'last_modified_by', 'authors']
    __field_order__ = ['title', 'abstract', 'content', 'language', 'parent', 'status', 'featured', 'created', 'published']
    __hidden_fields__ = ['page_id']
    __dropdown_field_names__ = ['title', 'user_name', 'name']
    content = CKEditor(id='content')
    title = TextField


class PageConfig(CrudRestControllerConfig):
    new_form_type = NewPageForm
    edit_form_type = EditPageForm
    table_filler_type = PageTableFiller

    class defaultCrudRestController(CrudRestController):

        #xxx: probably should become a post-commit hook on the object
        def _post_modify_page_obj(self, obj, **kw):
            if not kw.get('hidden', None):
                obj.slug = slugify(obj.title)[:255]

            obj.last_modified_by = request.identity['user']
            if obj.status_id == STATUS_PUBLISHED_ID and obj.published == None:
                obj.published = datetime.now()

            if obj.featured:
                featured_pages.clear()

        @expose(inherit=True)
        def put(self, *args, **kw):
            """update"""
            kw['page_id'] = args[0]

            omit_fields = [self.edit_form.__omit_fields__]

            obj = self.provider.update(self.model, params=kw, omit_fields=omit_fields)
            self._post_modify_page_obj(obj, **kw)

            redirect('../', params=self._kept_params())

        @expose(inherit=True)
        def post(self, *args, **kw):
            obj = self.provider.create(self.model, params=kw)
            self._post_modify_page_obj(obj, **kw)
            raise redirect('./', params=self._kept_params())