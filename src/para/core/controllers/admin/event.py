# -*- coding: utf-8 -*-
"""Event Admin Setup"""

from datetime import datetime
from slugify import slugify

from tg import redirect, expose, request

from sqlalchemy import desc
import transaction

from tw2.ckeditor import CKEditor
from tw2.forms import TextField, SubmitButton

from sprox.formbase import AddRecordForm, EditableForm
from sprox.fillerbase import TableFiller

from tgext.admin.config import CrudRestControllerConfig
from tgext.crud.controller import CrudRestController

from para.core.model.event import Event
from para.core.model.status import STATUS_PUBLISHED_ID
from para.core.model.event import current_events
from para.core.model import DBSession

class EventTableFiller(TableFiller):
    __model__ = Event

    def description(self, obj):
        return obj.description[:100] + '...'

    def meta_keywords(self, obj):
        if obj.meta_keywords and len(obj.meta_keywords)>100:
            return obj.meta_keywords[:100] + '...'
        return obj.meta_keywords

    def meta_description(self, obj):
        if obj.meta_description and len(obj.meta_description)>100:
            return obj.meta_description[:100] + '...'
        return obj.meta_description


from sprox.widgets import PropertySingleSelectField

class ParentEventField(PropertySingleSelectField):
    def prepare(self):

        events = DBSession.query(Event).order_by(desc(Event.datetime)).all()
        options = [(event.event_id, '[%s] %s'%(event.datetime and event.datetime.strftime('%m-%d-%Y') or "", event.name))
                            for event in events]

        self.options = options
        if self.nullable:
            self.options.append(['', "-----------"])

        if not self.value:
            self.value = ''

        super(PropertySingleSelectField, self).prepare()

class EditEventForm(EditableForm):
    __model__ = Event
    __omit_fields__ = ['children', 'last_modified', 'last_modified_by', 'users', 'created']
    __hide_fields__ = ['event_id']
    __field_order__ = ['name', 'description', 'location', 'url']
    description = CKEditor(id='description')
    name = TextField
    slug = TextField
    url = TextField(id='url', size=255)
    parent = ParentEventField


class NewEventForm(AddRecordForm):
    __model__ = Event
    __omit_fields__ = ['children', 'last_modified', 'last_modified_by', 'users', 'event_id', 'created']
    __field_order__ = ['name', 'description', 'location', 'url']
    description = CKEditor(id='description')
    name = TextField
    slug = TextField
    url = TextField(id='url', size=255)
    parent = ParentEventField


class EventConfig(CrudRestControllerConfig):
    new_form_type = NewEventForm
    edit_form_type = EditEventForm
    table_filler_type = EventTableFiller

    class defaultCrudRestController(CrudRestController):

        #xxx: probably should become a post-commit hook on the object
        def _post_modify_blog_obj(self, obj):
            slug = slugify(obj.name)[:255]

            if obj.datetime:
                slug = '-'.join((slug, obj.datetime.strftime('%m-%d-%Y')))

            obj.slug = slug
            current_events.clear()

        @expose('para.core.templates.admin.events.edit')
        def edit(self, *args, **kw):
            return CrudRestController.edit(self, *args, **kw)

        @expose()
        def clone(self, event_id, *args, **kw):
            clone = DBSession.query(Event).get(event_id)

            new_event = Event()
            new_event.language_id = clone.language_id
            new_event.created = datetime.now()
            new_event.location = clone.location
            new_event.url = clone.url
            new_event.description = clone.description
            new_event.meta_author = clone.meta_author
            new_event.meta_description = clone.meta_description
            new_event.meta_keywords = clone.meta_keywords
            new_event.name = clone.name
            new_event.datetime = clone.datetime
            new_event.slug = clone.slug
            new_event.users = clone.users
            DBSession.add(new_event)
            DBSession.flush()

            redirect('../%s/edit' % new_event.event_id)

        @expose(inherit=True)
        def put(self, *args, **kw):
            """update"""
            kw['blog_entry_id'] = args[0]

            omit_fields = [self.edit_form.__omit_fields__]

            obj = self.provider.update(self.model, params=kw, omit_fields=omit_fields)
            self._post_modify_blog_obj(obj)

            redirect('../', params=self._kept_params())

        @expose(inherit=True)
        def post(self, *args, **kw):
            obj = self.provider.create(self.model, params=kw)
            self._post_modify_blog_obj(obj)
            raise redirect('./', params=self._kept_params())
