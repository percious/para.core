# -*- coding: utf-8 -*-
"""User Admin Setup"""

from formencode.validators import FieldsMatch

from tw2.forms import PasswordField

from sprox.formbase import AddRecordForm, EditableForm
from sprox.tablebase import TableBase

from tgext.admin.config import CrudRestControllerConfig

from para.core.model.auth import User


class NewUserForm(AddRecordForm):
    __model__ = User
    __require_fields__ = ['password', 'user_name']
    __omit_fields__ = ['_password', 'groups', 'created', 'user_id', 'page_entries', 'blog_entries', 'events', 'user_id']
    __field_order__ = ['user_name', 'password', 'verify_password']
    __base_validator__ = FieldsMatch('password',
                                     'verify_password',
                                      messages={'invalidNoMatch':
                                     'Passwords do not match'})
    verify_password = PasswordField('verify_password')


class EditUserForm(EditableForm):
    __model__ = User
    __require_fields__ = ['password', 'user_name']
    __omit_fields__ = ['_password', 'groups', 'created', 'user_id', 'page_entries', 'blog_entries', 'events']
    __field_order__ = ['user_name', 'password', 'verify_password']
    __base_validator__ = FieldsMatch('password',
                                     'verify_password',
                                      messages={'invalidNoMatch':
                                     'Passwords do not match'})
    verify_password = PasswordField('verify_password')


class UserTable(TableBase):
    __model__ = User
    __omit_fields__ = ['blog_entries', 'pages', 'events']

class UserConfig(CrudRestControllerConfig):
    new_form_type = NewUserForm
    edit_form_type = EditUserForm
    table_type = UserTable


