# -*- coding: utf-8 -*-
"""event Controller"""
from datetime import datetime, timedelta
from tg import expose, abort, tmpl_context
from tg.i18n import get_lang, set_lang
from tg.controllers import RestController
from tg.decorators import with_trailing_slash
from para.core.model import DBSession,Language
from para.core.model.event import Event
from sqlalchemy import desc


class EventController(RestController):
    """
    Controller for events
    """

    def __init__(self, lang=None):
        self.lang = lang

    @with_trailing_slash
    @expose('para.core.templates.events.get_all')
    def get_all(self, **kwargs):

        lang = self.lang and DBSession.query(Language).filter(Language.code == self.lang).first()

        events = DBSession.query(Event).\
            filter(Event.datetime > (datetime.now() - timedelta(days=30))).\
            filter(Event.language == lang).\
            order_by(desc(Event.datetime))

        return dict(events=events)

    @expose('para.core.templates.events.get_one')
    def get_one(self, event_id):
        event = DBSession.query(Event).filter(Event.slug == event_id).first()
        if event is None:
            try:
                event_id = int(event_id)
                event = DBSession.query(Event).get(event_id)
            except ValueError:
                abort(404)
        if event is None:
            abort(404)
        return dict(event=event)
