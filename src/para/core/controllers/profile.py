# -*- coding: utf-8 -*-
"""Admin Setup"""

from datetime import datetime

from tg import redirect, expose, flash, tmpl_context
from tg.i18n import ugettext as _

from formencode.validators import FieldsMatch

from tw2.forms import PasswordField, TextField, HiddenField

from sprox.formbase import AddRecordForm, EditableForm
from sprox.fillerbase import EditFormFiller

from tgext.crud.controller import CrudRestController, registered_validate

from para.core.lib.decorators import https
from para.core.model.auth import User, Contact
from para.core.model import DBSession

contact_fields = [
'salutation',
'first_name',
'middle_name',
'last_name',
'middle_name',
'email_address',
'address',
'address2',
'city',
'state',
'country',
'zip',
'cell_phone',
'work_phone',
'home_phone',
'contact_id'
]

new_profile_fields = ['user_name', 'password', 'verify_password']
new_profile_fields.extend(contact_fields)


class NewProfileForm(AddRecordForm):
    __model__ = User
    __require_fields__ = ['password', 'user_name']
    __limit_fields__ = new_profile_fields

    verify_password = PasswordField('verify_password')
    salutation = TextField('salutation', label=_('Salutation'), placeholder='Mr., Ms., Dr. Etc.')
    first_name = TextField('first_name', label=_('First Name'))
    middle_name = TextField('middle_name', label=_('Middle Name'))
    last_name = TextField('last_name', label=_('Last Name'))
    email_address = TextField('email_address', label=_('Email Address'))
    address = TextField('address', label=_('Address'))
    address2 = TextField('address2', label=_('Address2'))
    city = TextField('city', label=_('City'))
    state = TextField('state', label=_('State'))
    country = TextField('country', label=_('Country'))
    zip = TextField('zip', label=_('Zipcode'))
    cell_phone = TextField('cell_phone', label=_('Cell Phone'))
    work_phone = TextField('work_phone', label=_('Work Phone'))
    home_phone = TextField('home_phone', label=_('Home Phone'))

    __base_validator__ = FieldsMatch('password',
                                     'verify_password',
                                      messages={'invalidNoMatch':
                                     'Passwords do not match'})

new_profile_form = NewProfileForm(DBSession)

edit_profile_fields = ['user_id', 'user_name', 'password', 'verify_password']
edit_profile_fields.extend(contact_fields)


class EditProfileForm(EditableForm):
    __model__ = User
    __hide_fields__ = ['user_id']
    __require_fields__ = ['user_name']
    __limit_fields__ = edit_profile_fields

    verify_password = PasswordField('verify_password')
    salutation = TextField('salutation', label=_('Salutation'), placeholder='Mr., Ms., Dr. Etc.')
    first_name = TextField('first_name', label=_('First Name'))
    middle_name = TextField('middle_name', label=_('Middle Name'))
    last_name = TextField('last_name', label=_('Last Name'))
    email_address = TextField('email_address', label=_('Email Address'))
    address = TextField('address', label=_('Address'))
    address2 = TextField('address2', label=_('Address2'))
    city = TextField('city', label=_('City'))
    state = TextField('state', label=_('State'))
    country = TextField('country', label=_('Country'))
    zip = TextField('zip', label=_('Zipcode'))
    cell_phone = TextField('cell_phone', label=_('Cell Phone'))
    work_phone = TextField('work_phone', label=_('Work Phone'))
    home_phone = TextField('home_phone', label=_('Home Phone'))
    contact_id = HiddenField('contact_id')

    __base_validator__ = FieldsMatch('password',
                                     'verify_password',
                                      messages={'invalidNoMatch':
                                     'Passwords do not match'})


edit_profile_form = EditProfileForm(DBSession)


class EditProfileFiller(EditFormFiller):
    __model__ = User

edit_profile_form_filler = EditProfileFiller(DBSession)


class ProfileController(CrudRestController):

    model = User
    new_form = new_profile_form
    edit_form = edit_profile_form
    edit_filler = edit_profile_form_filler
    table_filler = None

    #xxx: probably should become a post-commit hook on the object
    def _post_modify_contact_obj(self, obj):
        obj.last_modified = datetime.now()

    @https
    @expose('para.core.templates.profile.new')
    def new(self, *args, **kw):
        return super(ProfileController, self).new(*args, **kw)

    @https
    @expose('para.core.templates.profile.edit', inherit=True)
    def edit(self, *args, **kw):
        #only allow user to edit their own profile
        args = [tmpl_context.identity['user'].user_id]
        return super(ProfileController, self).edit(*args, **kw)

    @https
    @expose(inherit=True)
    @registered_validate(error_handler=edit)
    def put(self, *args, **kw):
        """update"""

        contact_args = dict(((key, value) for key, value in kw.iteritems() if key in contact_fields))
        user_args = dict(((key, value) for key, value in kw.iteritems() if key not in contact_fields))

        omit_fields = [self.edit_form.__omit_fields__]

        user = self.provider.update(User, params=user_args, omit_fields=omit_fields)
        contact = self.provider.update(Contact, params=contact_args, omit_fields=omit_fields)
        self._post_modify_contact_obj(contact)

        flash(_('Your profile information has been saved.'))

        raise redirect('/')

    @https
    @expose(inherit=True)
    def post(self, *args, **kw):

        contact_args = dict(((key, value) for key, value in kw.iteritems() if key in contact_fields))
        user_args = dict(((key, value) for key, value in kw.iteritems() if key not in contact_fields))

        contact = self.provider.create(Contact, params=contact_args)

        user_args['contact'] = contact
        user = self.provider.create(User, params=user_args)

        self._post_modify_contact_obj(contact)
        flash(_('Thank you for your entry, you will be notified when your account is activated.'))

        raise redirect('/')

    def get_all(self, *args, **kw):
        pass
    def get_one(self, *args, **kw):
        pass
    def delete(self, *args, **kw):
        pass