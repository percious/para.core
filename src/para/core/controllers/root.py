# -*- coding: utf-8 -*-
"""Main Controller"""

from datetime import datetime

from sqlalchemy import desc, or_
from sqlalchemy.orm.exc import NoResultFound
from sqlalchemy.orm import joinedload

from tg import predicates, abort
from tg import expose, flash, require, request, redirect, lurl, app_globals
from tg.i18n import ugettext as _

from turbomail import Message

from para.core import model as para_model
from para.core.model import DBSession

from para.core.controllers.blog import BlogController
from para.core.controllers.events import EventController
from para.core.controllers.error import ErrorController
from para.core.controllers.admin import AdminController, AdminConfig

from para.core.lib.base import BaseController, url
from para.core.lib.decorators import https

from para.core.model.inquiry import Inquiry
from para.core.model.blog import BlogEntry
from para.core.model.page import Page
from para.core.model.language import Language
from para.core.model.status import STATUS_PUBLISHED_ID
from para.core.model.event import Event
from para.core.model.settings import settings

from para.core.controllers.profile import ProfileController

__all__ = ['RootController']


class ParaController(BaseController):
    """
    The root controller for the para application.
    """

    model = para_model

    event_controller = EventController
    blog_controller = BlogController

    def __init__(self, lang=None):
        if lang is None:
            self.admin = AdminController(self.model, DBSession, config_type=AdminConfig)
            self.error = ErrorController()
            self.profile = ProfileController(DBSession)
            languages = getattr(app_globals, 'languages', [])
            for language in languages:
                setattr(self, language, self.__class__(language))
        self.lang = lang

        self.blog = self.blog_controller(lang)
        self.events = self.event_controller(lang)

    @expose('para.core.templates.page')
    def _default(self, *args, **kwargs):
        try:
            eng_page = DBSession.query(Page).filter(Page.slug == '/'.join(args)).\
                filter(Page.status_id == STATUS_PUBLISHED_ID)

            if self.lang:
                l = DBSession.query(Language).filter(Language.code == self.lang).first()
                page = eng_page.filter(Page.language == l).first()
                if not page:
                    page = eng_page.options(joinedload('children')).first()
                    if page:
                        for child in page.children:
                            # the page has a child for the appropriate language
                            if child.language_id == l.language_id and child.status_id == STATUS_PUBLISHED_ID:
                                raise redirect(url(child.slug))#, redirect_with=HTTPMovedPermanently)
                else:
                    page = eng_page.one()
            else:
                page = eng_page.one()
                if page.language is not None and page.parent:
                    # redirect to the english page if you are on another
                    # language page and the english page exists
                    raise redirect(url(page.parent.slug))#, redirect_with=HTTPMovedPermanently)
        except NoResultFound:
            return abort(404)

        if page is None:
            return abort(404)

        return {'page': page, 'menu': page.menu}

    @expose('para.core.templates.index')
    def index(self):
        """Handle the front-page."""
        featured_blog_entry_q = DBSession.query(BlogEntry).\
            filter(BlogEntry.status_id == STATUS_PUBLISHED_ID).\
            order_by(desc(BlogEntry.published)).order_by(BlogEntry.featured)

        if self.lang:
           featured_blog_entry_q = featured_blog_entry_q.join(Language).filter(Language.code == self.lang)
        else:
           featured_blog_entry_q = featured_blog_entry_q.filter(BlogEntry.language == None)

        featured_blog_entry = featured_blog_entry_q.first()

        index_page = None
        if self.lang:
            index_page = DBSession.query(Page).filter(Page.slug == 'index').join(Language).filter(Language.code == self.lang).first()
        if index_page is None:
            index_page = DBSession.query(Page).filter(Page.slug == 'index').filter(Page.language == None).first()
        if index_page is None:
            index_page = ''
        return dict(page='index',
                    featured_blog_entry=featured_blog_entry,
                    index_page=index_page
        )


    @expose()
    def inquire(self, comment, phone, name, email, **kwargs):

        inquiry = Inquiry(comment=comment,
                          phone=phone,
                          name=name,
                          email=email)

        DBSession.add(inquiry)

        message = Message(settings['email'], settings['email'], 'New inquiry from %s' % name)

        message.plain = """There was a new inquiry from: %s.
email: %s
phone: %s
comment: %s""" %(name, email, phone, comment)

        message.send()

        flash("Thank you for your submission. We will follow up with you shortly.")
        return redirect('/')

    @expose('para.core.templates.contact')
    def contact(self):
        return dict(menu='contact')

    @require(predicates.not_anonymous())
    @expose('para.core.templates.clients')
    def clients(self):
        return dict(menu='clients')

    #@expose('para.core.templates.index')
    #@require(predicates.is_user('editor', msg=l_('Only for the editor')))
    #def editor_user_only(self, **kw):
    #    """Illustrate how a page exclusive for the editor works."""
    #    return dict(page='editor stuff')

    @https
    @expose('para.core.templates.login')
    def login(self, came_from=lurl('/')):
        """Start the user login."""
        login_counter = request.environ.get('repoze.who.logins', 0)
        if login_counter > 0:
            flash(_('Wrong credentials'), 'warning')
        return dict(page='login', login_counter=str(login_counter),
                    came_from=came_from, menu='clients')

    @https
    @expose()
    def post_login(self, came_from=lurl('/')):
        """
        Redirect the user to the initially requested page on successful
        authentication or redirect her back to the login page if login failed.

        """
        if not request.identity:
            login_counter = request.environ.get('repoze.who.logins', 0) + 1
            redirect(url('/login'),
                params=dict(came_from=came_from, __logins=login_counter))
        userid = request.identity['repoze.who.userid']
        flash(_('Welcome back, %s!') % userid)
        redirect(came_from)

    @expose()
    def post_logout(self, came_from=lurl('/')):
        """
        Redirect the user to the initially requested page on logout and say
        goodbye as well.

        """
        flash(_('We hope to see you soon!'))
        redirect(came_from)

    @expose('para.core.templates.sitemap', content_type='text/html')
    @expose('para.core.templates.sitemap_xml', content_type='application/xml')
    @expose('para.core.templates.sitemap_xml', content_type='text/xml')
    def sitemap(self):
        languages = getattr(app_globals, 'languages', [])
        a = DBSession.query(Page.slug, Page.last_modified, Page.title, Page.language, Language.code).\
                join(Page.language).\
                filter(or_(Page.hidden == None, Page.hidden == False)).\
                filter(Page.status_id == STATUS_PUBLISHED_ID)

        #Page.menu is a hack, it is used to match up with Language.code and is not used in the sitemap
        b = DBSession.query(Page.slug, Page.last_modified, Page.title, Page.language, Page.menu).\
                filter(or_(Page.hidden == None, Page.hidden == False)).\
                filter(Page.status_id == STATUS_PUBLISHED_ID).\
                filter(Page.language_id == None)

        pages = a.union(b).order_by(desc(Page.last_modified)).all()


        a = DBSession.query(BlogEntry.slug, BlogEntry.last_modified, BlogEntry.title, BlogEntry.language, Language.code).\
            join(BlogEntry.language).\
            filter(BlogEntry.status_id == STATUS_PUBLISHED_ID)

        # Blog.meta_author is a hack to match up with Language.code, it's not used in the sitemap
        b = DBSession.query(BlogEntry.slug, BlogEntry.last_modified, BlogEntry.title, BlogEntry.language, BlogEntry.meta_author).\
            filter(BlogEntry.status_id == STATUS_PUBLISHED_ID).\
            filter(BlogEntry.language_id == None)

        blogs = a.union(b).order_by(desc(BlogEntry.last_modified)).all()

        events = DBSession.query(Event.slug, Event.name, Event.created).order_by(Event.created).all()

        about_page_modified = DBSession.query(Page.last_modified).\
            filter(Page.slug == u'about').first()

        if about_page_modified:
            about_page_modified = about_page_modified.last_modified
        else:
            about_page_modified = datetime.now()

        entities = []

        if blogs:
            entities.append(blogs[0].last_modified)
        if pages:
            entities.append(pages[0].last_modified)
        if events:
            entities.append(events[0].created)

        most_recent_modification = sorted(entities, reverse=True)

        if most_recent_modification:
            most_recent_modification = most_recent_modification[0]
        else:
            most_recent_modification = datetime.now()



        return {'pages': pages,
                'blogs': blogs,
                'events': events,
                'languages': languages,
                'about_page_modified': about_page_modified.strftime("%Y-%m-%d"),
                'most_recent_modification': most_recent_modification.strftime("%Y-%m-%d")
                }


class RootController(ParaController):
    pass
