# -*- coding: utf-8 -*-
"""Blog Controller"""
from sqlalchemy import desc
from sqlalchemy.orm import joinedload
from webhelpers.feedgenerator import Atom1Feed, RssFeed, Rss201rev2Feed

from tg import request, expose, abort, redirect, response
from tg.controllers import RestController
from tg.decorators import with_trailing_slash

from para.core.model import DBSession, BlogEntry
from para.core.model.status import STATUS_PUBLISHED_ID
from para.core.model.settings import settings
from para.core.model.language import Language
from para.core.lib.base import url


class BlogController(RestController):
    """
    Controller for Blog entries
    """

    def __init__(self, lang=None):
        self.lang = lang

    def _get_all_blogs(self):
        blogs = DBSession.query(BlogEntry).\
            filter(BlogEntry.status_id == STATUS_PUBLISHED_ID).\
            order_by(desc(BlogEntry.published))

        if self.lang:
            l = DBSession.query(Language).filter(Language.code == self.lang).first()
            if l:
                blogs = DBSession.query(BlogEntry).\
                    filter(BlogEntry.status_id == STATUS_PUBLISHED_ID).\
                    order_by(desc(BlogEntry.published)).\
                    filter(BlogEntry.language_id == l.language_id)
        else:
            blogs = blogs.filter(BlogEntry.language == None)
        return blogs

    @with_trailing_slash
    @expose('para.core.templates.blog.get_all')
    def get_all(self, **kwargs):
        blogs = self._get_all_blogs()
        return dict(blogs=blogs, menu='blog')

    @expose('para.core.templates.blog.get_one')
    def get_one(self, blog_entry_id):

        eng_blog = DBSession.query(BlogEntry).\
            filter(BlogEntry.slug == blog_entry_id).\
            filter(BlogEntry.status_id == STATUS_PUBLISHED_ID)

        if self.lang:
            l = DBSession.query(Language).filter(Language.code == self.lang).first()
            blog = eng_blog.filter(BlogEntry.language == l).first()
            if not blog:
                blog = eng_blog.options(joinedload('children')).first()
                if blog:
                    for child in blog.children:
                        # the blog has a child for the appropriate language
                        if child.language_id == l.language_id and child.status_id == STATUS_PUBLISHED_ID:
                            raise redirect(url('blog/' + child.slug))#, redirect_with=HTTPMovedPermanently)
            else:
                blog = eng_blog.first()
        else:
            blog = eng_blog.first()
            if blog is None:
                abort(404)

            if blog.language is not None and blog.parent:
                # redirect to the english blog if you are on another
                # language blog and the english blog exists
                raise redirect(url('blog/' + blog.parent.slug))#, redirect_with=HTTPMovedPermanently)

        if blog is None:
            abort(404)

        return dict(blog=blog, menu='blog')

    @expose()
    def atom1(self):
        """Produce an atom-1.0 feed via feedgenerator module"""
        response.content_type = 'application/atom+xml'
        return self._generate_feed(Atom1Feed)

    @expose()
    def rss(self):
        """Produce an RSS-2.0 feed via feedgenerator module"""
        response.content_type = 'application/rss+xml'
        return self._generate_feed(Rss201rev2Feed)

    def _generate_feed(self, FeedClass):

        feed = FeedClass(
            title=''.join((settings['company_name'], ' ', settings['city'], ', ', settings['state'], ' - Blog')),
            link=request.url,
            description=settings.get('blog_meta_description', "Blog"),
            language=u"en",
        )

        blog_url = request.url[:request.url.rfind('/')]
        blogs = self._get_all_blogs()

        for blog in blogs:
            feed.add_item(title=blog.title,
                          link='/'.join((blog_url, blog.slug)),
                          description=blog.abstract or blog.content[:250],
                          pubdate=blog.published,

                          )

        return feed.writeString('utf-8')