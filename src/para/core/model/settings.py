"""
Status Model
"""
from time import time

from sqlalchemy import Column
from sqlalchemy.types import Unicode
from sqlalchemy.orm.exc import NoResultFound

from para.core.model import DeclarativeBase, DBSession


class Setting(DeclarativeBase):
    """
    Status definition
    """

    __tablename__ = 'settings'

    name = Column(Unicode(50), primary_key=True)
    value = Column(Unicode(1023))


class Settings(dict):
    """
    Class get all settings as a dict, will refresh them every `timeout` seconds

    """

    def __init__(self, timeout=3):
        self._timeout = timeout
        self._last_access = 0
        self._data = {}

    def __getitem__(self, item):
        now = time()
        if self._last_access + self._timeout < now:
            self._last_access = now

            settings = DBSession.query(Setting).all()
            self._data = dict((s.name, s.value) for s in settings)

        return self._data[item]

    def __setitem__(self, key, value):
        try:
            setting = DBSession.query(Setting).filter(Setting.name == key).one()
            setting.value = value
        except NoResultFound:
            DBSession.add(Setting(name=key, value=value))
        self._data[key] = value


    def get(self, key, alt=None):
        try:
            return self[key]
        except (KeyError, IndexError):
            return alt

settings = Settings()