# -*- coding: utf-8 -*-
"""
Auth* related model.

This is where the models used by the authentication stack are defined.

It's perfectly fine to re-use this definition in the para application,
though.

"""
import os
from datetime import datetime
from hashlib import sha256

__all__ = ['User', 'Group', 'Permission']

from sqlalchemy import Table, ForeignKey, Column, func
from sqlalchemy.types import Unicode, Integer, DateTime, String
from sqlalchemy.orm import relation, synonym

from para.core.model import DeclarativeBase, metadata, DBSession


class Contact(DeclarativeBase):
    __tablename__ = 'contacts'

    contact_id = Column(Integer, autoincrement=True, primary_key=True)
    salutation = Column(Unicode(4))
    first_name = Column(Unicode(31))
    middle_name = Column(Unicode(31))
    last_name = Column(Unicode(100))
    middle_name = Column(Unicode(31))
    email_address = Column(Unicode(255), unique=True, nullable=False)
    address = Column(Unicode(100))
    address2 = Column(Unicode(100))
    city = Column(Unicode(63))
    state = Column(String(7))
    country = Column(Unicode(31))
    zip = Column(String(12))

    cell_phone = Column(String(12))
    work_phone = Column(String(12))
    home_phone = Column(String(12))

    last_modified = Column(DateTime, onupdate=func.current_timestamp())
