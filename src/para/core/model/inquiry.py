"""
Status Model
"""
from datetime import datetime
from sqlalchemy import Column
from sqlalchemy.types import Unicode, Integer, DateTime

from para.core.model import DeclarativeBase


class Inquiry(DeclarativeBase):
    """
    Status definition
    """

    __tablename__ = 'inquiries'

    inquiry_id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(Unicode(255))
    phone = Column(Unicode(25))
    email = Column(Unicode(255))
    comment = Column(Unicode(1024))
    created = Column(DateTime, default=datetime.now)
