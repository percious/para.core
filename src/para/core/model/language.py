"""
Language Model
"""
from sqlalchemy import Column
from sqlalchemy.types import Unicode, Integer

from para.core.model import DeclarativeBase


class Language(DeclarativeBase):
    """
    Blog Entry definition
    """

    __tablename__ = 'languages'

    language_id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(Unicode(50))
    description = Column(Unicode(250))
    code = Column(Unicode(2))