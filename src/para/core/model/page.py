"""
Page Model
"""

from sqlalchemy import Column, ForeignKey, func, Table
from sqlalchemy.types import Unicode, Integer, String, Text, DateTime, Boolean
from sqlalchemy.orm import relation, backref, joinedload

from para.core.model import DeclarativeBase, metadata
from para.core.model.status import Status
from para.core.model.language import Language
from para.core.model.auth import User
from para.core.model import DBSession
from para.core.lib.cache import TimedObjectCache


user_page_table = Table('user_pages', metadata,
                        Column('user_id', Integer,
                               ForeignKey('users.user_id',
                                          onupdate="CASCADE",
                                          ondelete="CASCADE"),
                               primary_key=True),
                        Column('page_id', Integer,
                               ForeignKey('pages.page_id',
                                          onupdate="CASCADE",
                                          ondelete="CASCADE"),
                               primary_key=True)
)


class Page(DeclarativeBase):
    """
    page Entry definition
    """

    __tablename__ = 'pages'

    page_id = Column(Integer, autoincrement=True, primary_key=True, index=True)
    parent_page_id = Column(Integer,
                            ForeignKey('pages.page_id'))
    title = Column(Unicode(200))
    slug = Column(String(200), index=True)
    menu = Column(String(50))
    content = Column(Text)
    created = Column(DateTime, default=func.now())
    published = Column(DateTime, default=func.now())
    last_modified = Column(DateTime, default=func.now(), onupdate=func.current_timestamp())
    last_modified_by_id = Column(Integer, ForeignKey('users.user_id'))
    language_id = Column(Integer, ForeignKey('languages.language_id'))
    status_id = Column(Integer, ForeignKey('statuses.status_id'))
    slug = Column(Unicode(255))

    #used for ordering on the main page
    priority = Column(Integer)
    featured = Column(Boolean)

    #hide the page from search engines
    hidden = Column(Boolean())

    meta_description = Column(Unicode(1024))
    meta_keywords = Column(Unicode(1024))
    meta_author = Column(Unicode(256))

    last_modified_by = relation(User)

    status = relation(Status)
    language = relation(Language)
    authors = relation(User, secondary=user_page_table,
                       backref='pages')

    children = relation("Page",
                        backref=backref('parent', remote_side=[page_id]))


class FeaturedPages(TimedObjectCache):
    """
    Class get all featured pages list, will refresh them every `timeout` seconds

    """

    def _do_get_objects(self, now, language):

        if language is None:
            return DBSession.query(Page).\
                filter(Page.featured == True).\
                filter(Page.language == None).\
                order_by(Page.priority).all()

        pages = DBSession.query(Page).\
            filter(Page.featured == True).\
            filter(Page.language == None).\
            options(joinedload('children')).\
            order_by(Page.priority).all()

        featured_pages = []
        for page in pages:
            child_page = None
            for child in page.children:
                if child.language == language:
                    child_page = child
            if child_page:
                featured_pages.append(child_page)
            else:
                featured_pages.append(page)
        return featured_pages


featured_pages = FeaturedPages(9999)