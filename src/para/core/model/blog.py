"""
Blog Models
"""
from sqlalchemy import Table, ForeignKey, Column, func
from sqlalchemy.types import Unicode, Integer, DateTime, Text, Boolean
from sqlalchemy.orm import relation, backref

from para.core.model import DeclarativeBase, metadata
from para.core.model.auth import User
from para.core.model.status import Status
from para.core.model.language import Language

user_blog_entries_table = Table('user_blog_entries', metadata,
                                Column('user_id', Integer,
                                       ForeignKey('users.user_id',
                                                  onupdate="CASCADE",
                                                  ondelete="CASCADE"),
                                       primary_key=True),
                                Column('blog_entry_id', Integer,
                                       ForeignKey('blog_entries.blog_entry_id',
                                                  onupdate="CASCADE",
                                                  ondelete="CASCADE"),
                                       primary_key=True)
)


class BlogEntry(DeclarativeBase):
    """
    Blog Entry definition
    """

    __tablename__ = 'blog_entries'

    blog_entry_id = Column(Integer, autoincrement=True, primary_key=True)
    parent_blog_entry_id = Column(Integer,
                                  ForeignKey('blog_entries.blog_entry_id'))
    title = Column(Unicode(255))
    abstract = Column(Text)
    content = Column(Text)
    created = Column(DateTime, default=func.now())
    published = Column(DateTime)
    featured = Column(Boolean)
    last_modified = Column(DateTime, default=func.now(), onupdate=func.current_timestamp())
    last_modified_by_id = Column(Integer, ForeignKey('users.user_id'))
    language_id = Column(Integer, ForeignKey('languages.language_id'))
    status_id = Column(Integer, ForeignKey('statuses.status_id'))
    slug = Column(Unicode(255))

    meta_description = Column(Unicode(1024))
    meta_keywords = Column(Unicode(1024))
    meta_author = Column(Unicode(255))

    last_modified_by = relation(User)
    status = relation(Status)
    language = relation(Language)
    authors = relation(User, secondary=user_blog_entries_table,
                       backref='blog_entries')

    children = relation("BlogEntry",
                        backref=backref('parent', remote_side=[blog_entry_id]))
