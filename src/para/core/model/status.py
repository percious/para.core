"""
Status Model
"""
from sqlalchemy import Column
from sqlalchemy.types import Unicode, Integer

from para.core.model import DeclarativeBase

STATUS_PUBLISHED_ID = 3


class Status(DeclarativeBase):
    """
    Status definition
    """

    __tablename__ = 'statuses'

    status_id = Column(Integer, autoincrement=True, primary_key=True)
    name = Column(Unicode(50))
    description = Column(Unicode(250))