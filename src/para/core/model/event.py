"""
Blog Models
"""
from datetime import datetime, timedelta

from sqlalchemy import Table, ForeignKey, Column, func
from sqlalchemy.types import Unicode, Integer, DateTime, String
from sqlalchemy.orm import relation, backref, joinedload

from para.core.model import DeclarativeBase, metadata, DBSession
from para.core.model.auth import User
from para.core.model.language import Language
from para.core.lib.cache import TimedObjectCache

user_events_table = Table('user_events', metadata,
                          Column('user_id', Integer,
                                 ForeignKey('users.user_id',
                                            onupdate="CASCADE",
                                            ondelete="CASCADE"),
                                 primary_key=True),
                          Column('event_id', Integer,
                                 ForeignKey('events.event_id',
                                            onupdate="CASCADE",
                                            ondelete="CASCADE"),
                                 primary_key=True)
                          )

from settings import settings

class Event(DeclarativeBase):
    """
    Event definition
    """

    __tablename__ = 'events'

    event_id = Column(Integer, autoincrement=True, primary_key=True)
    parent_event_id = Column(Integer,
                             ForeignKey('events.event_id'))
    name = Column(Unicode(100))
    description = Column(Unicode(1024))
    location = Column(Unicode(300))
    datetime = Column(DateTime)
    created = Column(DateTime, default=func.now())
    language_id = Column(Integer, ForeignKey('languages.language_id'))
    slug = Column(String(255))
    url = Column(String(300))

    meta_description = Column(Unicode(1024))
    meta_keywords = Column(Unicode(1024))
    meta_author = Column(Unicode(256))

    language = relation(Language)
    users = relation(User, secondary=user_events_table,
                     backref='events')

    children = relation("Event",
                        backref=backref('parent', remote_side=[event_id]))


class CurrentEvents(TimedObjectCache):

    def _do_get_objects(self, now, language):
        now = datetime.now()
        events = []
        if language is None:
            events = DBSession.query(Event).\
                filter(Event.datetime > now - timedelta(days=30)).\
                filter(Event.datetime < (now + timedelta(days=150))).\
                filter(Event.language == None).\
                order_by(Event.datetime.desc()).all()
        else:
            events = DBSession.query(Event).\
                filter(Event.datetime > now - timedelta(days=30)).\
                filter(Event.datetime < (now + timedelta(days=150))).\
                filter(Event.language == None).\
                options(joinedload('children')).\
                order_by(Event.datetime.desc()).all()


            #swap out associated current events of the requested language
            current_events = []
            for event in events:
                child_event = None
                for child in event.children:
                    if child.language == language:
                        child_event = child
                        continue
                if child_event:
                    current_events.append(child)
                else:
                    current_events.append(event)
            events = current_events

        display_event_number = settings.get('display_event_number', 5)

        def delta(a, b):
            diffa = abs((now - a.datetime).days)
            diffb = abs((now-b.datetime).days)

            if diffa > diffb:
                return 1
            return -1

        def datetime_ordered(a, b):

            if a.datetime > b.datetime:
                return -1
            return 1

        if len(events) > display_event_number:
            events = sorted(events, delta)
            events = events[:display_event_number]


            events = sorted(events, datetime_ordered)

        return events


current_events = CurrentEvents(60)