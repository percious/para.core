# -*- coding: utf-8 -*-
"""
Global configuration file for TG2-specific settings in para.

This file complements development/deployment.ini.

Please note that **all the argument values are strings**. If you want to
convert them into boolean, for example, you should use the
:func:`paste.deploy.converters.asbool` function, as in::
    
    from paste.deploy.converters import asbool
    setting = asbool(global_conf.get('the_setting'))
 
"""
import atexit
import re

import logging as log
import os.path
import imp
from tg.configuration import AppConfig
from tg.configuration.app_config import config
from tg.configuration.auth import TGAuthMetadata
from tg.render import render_mako
from tg.support.middlewares import StaticsMiddleware
from tg import config
from tg.i18n import set_lang
from turbomail import interface

import para.core
from para.core.lib.templates import ParaDottedFileNameFinder, ParaDottedTemplateLookup
from para.core import model

class FakeConfigObj(object):
    """TODO: Docstring incomplete."""

    def __init__(self, real_config):
        self._config = real_config
        self._nr_regex = re.compile('^(\d+)$')

    def get(self, option, default):
        value = self._config.get(option, default)
        return self._convert(option, value)

    def _convert(self, option, value):
        if value is not None:
            boolean_options = (
                    'mail.smtp.tls',
                    'mail.tls',
                    'mail.smtp.debug',
                    'mail.debug'
                )

            should_be_bool = (option.endswith('.on') or (option in boolean_options))

            if should_be_bool:
                value = value == 'True' and True or False

            elif hasattr(value, 'isdigit') and value.isdigit():
                value = int(value)

        return value

    def update(self, new_value_dict):
        self._config.update(new_value_dict)


def start_mail_extension():
    # There is no guarantee that atexit calls shutdown but Pylons does not
    # provide other mechanisms!
    atexit.register(shutdown_mail_extension)

    # interface.start will exit immediately if TurboMail is not enabled.

    interface.start(FakeConfigObj(config))


def shutdown_mail_extension():
    interface.stop()


class ParaAppConfig(AppConfig):

    def setup_helpers_and_globals(self):
        """Add helpers and globals objects to the config.

        Override this method to customize the way that ``app_globals``
        and ``helpers`` are setup.

        """

        try:
            from para.core.lib import app_globals
            g = app_globals.Globals()
        except AttributeError:
            log.warn('Application has a package but no lib.app_globals.Globals class is available.')
            return

        g.dotted_filename_finder = ParaDottedFileNameFinder()

        config['tg.app_globals'] = g

        if config.get('tg.pylons_compatible', True):
            config['pylons.app_globals'] = g

        start_mail_extension()

    def add_static_file_middleware(self, app):

        app = StaticsMiddleware(app, os.path.join(os.path.dirname(para.core.__file__), 'public'))

        local_package = config.get('para.package', None)
        if local_package:
            app = StaticsMiddleware(app, os.path.join(imp.find_module(local_package)[1], 'public'))

        return app

    def setup_mako_renderer(self, use_dotted_templatenames=None):
        """Setup a renderer and loader for mako templates.

        """

        # If no dotted names support was required we will just setup
        # a file system based template lookup mechanism.
        compiled_dir = config.get('templating.mako.compiled_templates_dir', None)

        if not compiled_dir or compiled_dir.lower() in ('none', 'false'):
            # Cache compiled templates in-memory
            compiled_dir = None
        else:
            bad_path = None
            if os.path.exists(compiled_dir):
                if not os.access(compiled_dir, os.W_OK):
                    bad_path = compiled_dir
                    compiled_dir = None
            else:
                try:
                    os.makedirs(compiled_dir)
                except:
                    bad_path = compiled_dir
                    compiled_dir = None
            if bad_path:
                log.warn("Unable to write cached templates to %r; falling back "
                         "to an in-memory cache. Please set the `templating.mak"
                         "o.compiled_templates_dir` configuration option to a "
                         "writable directory." % bad_path)

        config['tg.app_globals'].mako_lookup = ParaDottedTemplateLookup(
            input_encoding='utf-8', output_encoding='utf-8',
            imports=['from markupsafe import escape_silent as escape'],
            module_directory=compiled_dir,
            default_filters=['escape'],
            auto_reload_templates=self.auto_reload_templates)

        self.render_functions.mako = render_mako

base_config = ParaAppConfig()
base_config.renderers = []
base_config.prefer_toscawidgets2 = True

base_config.package = para.core

#Enable json in expose
base_config.renderers.append('json')
#Enable genshi in expose to have a lingua franca for extensions and pluggable apps
#you can remove this if you don't plan to use it.
base_config.renderers.append('genshi')

#Set the default renderer
base_config.default_renderer = 'mako'
base_config.renderers.append('mako')
#Configure the base SQLALchemy Setup
base_config.use_sqlalchemy = True
base_config.model = model
base_config.DBSession = para.core.model.DBSession
# Configure the authentication backend

# YOU MUST CHANGE THIS VALUE IN PRODUCTION TO SECURE YOUR APP 
base_config.sa_auth.cookie_secret = "8530a814-edc2-4525-9f5a-8b747e12c53a"

base_config.auth_backend = 'sqlalchemy'

# what is the class you want to use to search for users in the database
base_config.sa_auth.user_class = model.User

from tg.configuration.auth import TGAuthMetadata

#This tells to TurboGears how to retrieve the data for your user
class ApplicationAuthMetadata(TGAuthMetadata):
    def __init__(self, sa_auth):
        self.sa_auth = sa_auth
    def authenticate(self, environ, identity):
        user = self.sa_auth.dbsession.query(self.sa_auth.user_class).filter_by(user_name=identity['login']).first()
        if user and user.validate_password(identity['password']):
            return identity['login']
    def get_user(self, identity, userid):
        return self.sa_auth.dbsession.query(self.sa_auth.user_class).filter_by(user_name=userid).first()
    def get_groups(self, identity, userid):
        return [g.group_name for g in identity['user'].groups]
    def get_permissions(self, identity, userid):
        return [p.permission_name for p in identity['user'].permissions]

base_config.sa_auth.dbsession = model.DBSession

base_config.sa_auth.authmetadata = ApplicationAuthMetadata(base_config.sa_auth)

# You can use a different repoze.who Authenticator if you want to
# change the way users can login
#base_config.sa_auth.authenticators = [('myauth', SomeAuthenticator()]

# You can add more repoze.who metadata providers to fetch
# user metadata.
# Remember to set base_config.sa_auth.authmetadata to None
# to disable authmetadata and use only your own metadata providers
#base_config.sa_auth.mdproviders = [('myprovider', SomeMDProvider()]

# override this if you would like to provide a different who plugin for
# managing login and logout of your application
base_config.sa_auth.form_plugin = None

# You may optionally define a page where you want users to be redirected to
# on login:
base_config.sa_auth.post_login_url = '/post_login'

# You may optionally define a page where you want users to be redirected to
# on logout:
base_config.sa_auth.post_logout_url = '/post_logout'
