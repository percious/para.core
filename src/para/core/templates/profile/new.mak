<%inherit file="para.core.templates.master"/>

<%def name="title()">
    Create a new Profile
</%def>

<div id="main_content">
  <div id="crud_content">
    <div class="crud_add">
      <h2>Create a New Profile</h2>
       ${tmpl_context.widget(value=value, action='./') | n}
    </div>
  </div>
  <div style="clear:both;"> &nbsp; </div>
</div>
