<%inherit file="para.core.templates.master"/>

<%def name="title()">
    Edit Your Profile
</%def>

<div id="main_content">
  <div id="crud_content">
    <div class="crud_add">
      <h2>Edit Your Profile</h2>
       ${tmpl_context.widget(value=value, action='./') | n}
    </div>
  </div>
  <div style="clear:both;"> &nbsp; </div>
</div>
