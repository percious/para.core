<%inherit file="para.core.templates.master"/>

<%def name="title()">
<%
    settings = tmpl_context.settings
%>

${settings['company_name']} - ${settings['city']}, ${settings['state']} - Contact
</%def>

<%
    settings = tmpl_context.settings
%>


<h3>${settings['company_name']}<br/></h3>
${settings['address']} ${settings['city']}, ${settings['state']} ${settings['zip']}<br/>
<span>phone:</span> <a href="tel:${settings['phone']}">${settings['phone']}</a><br/>
fax: ${settings['fax']}<br/>
email: <a href="mailto:${settings['email']}">${settings['email']}</a><br/>
<br/>


<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
        src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=${settings['address'].replace(' ', '+').lower()},+${settings['city']}+${settings['state']}&amp;aq=&amp;&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=${settings['address'].replace(' ', '+').lower()},+${settings['city']}+${settings['state']}+${settings['zip']}&amp;z=14&amp;output=embed"></iframe>
