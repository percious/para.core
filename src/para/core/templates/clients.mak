<%inherit file="para.core.templates.master"/>

<%def name="title()">
    Clients only
</%def>

<h2><span>Client</span> System</h2>

<ul>
<li>Edit your <a href="${url('/profile/edit')}">Profile</a>.</li>
%if request.identity and 'managers' in request.identity['groups']:
    <li>Enter the <a href="/admin/" title="">Admin</a> System.</li>
%endif
</ul>