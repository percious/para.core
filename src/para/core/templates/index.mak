<%inherit file="para.core.templates.master"/>

<%def name="title()">
  Para Law Business Management Software
</%def>

  <div class="row">
    <div class="span12">
      <div class="hero-unit" style="height:230px">
        <div style="float:left; width:550px">
          <h1>Para</h1>
          <p>Hi, I'm para.core.</p>
          <div style="border: float:left; width:500px;"> I'm here to help you get the word out for your law firm, land clients, and
           reduce your overhead so you can focus on the important part of your business.
          </div>
          <div>
          <a class="btn btn-primary btn-large" href="http://www.turbogears.org" target="_blank">
            ##${h.icon('book', True)} Learn more
          </a>
         </div>
       </div>
       <div style="padding:0px; width:250px; float:right">
           <img width="200" src="/images/15046660-modern-business-woman-vector-illustration.jpg">
       </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="span4">
      <h3>Blog about your firm.</h3>
      <p>
      Report important <code>news</code>. Provide <code>insights</code> you for your area of law.
      Express your expert <code>opinions</code>.
      </p>
    </div>

    <div class="span4">
      <h3>Build the site you want.</h3>
      <p>
        Easily add <code>content</code> that is relevent to your business.
        <code>Present</code> your site your way.
      </p>
    </div>

    <div class="span4">
      <h3>Manage your clients.</h3>
      <p>Provide <code>clients</code> an easy and secure way to update their contact information online.
          Integrate a safe and secure <code>payment</code> system.</p>
    </div>
  </div>
  <p></p>
  <div class="notice"> Thank you for choosing para.core.</div>
