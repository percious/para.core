<%inherit file="para.core.templates.master"/>

<%def name="meta_description()">${tmpl_context.settings.get('blog_meta_description', 'Blog')}</%def>
<%def name="meta_keywords()">${tmpl_context.settings.get('blog_meta_keywords', 'blog, law, lawyer')}</%def>
<%def name="meta_extra()">
<%
    settings = tmpl_context.settings
%>
    <link rel="alternate" type="application/rss+xml" title="${settings['company_name']} - ${settings['city']}, ${settings['state']} - Blog (RSS 2.0)" href="${request.url+'rss.xml'}" />
    <link rel="alternate" type="application/atom+xml" title="${settings['company_name']} - ${settings['city']}, ${settings['state']} - Blog (Atom 1.0)" href="${request.url+'atom1.xml'}" />
</%def>
<%def name="title()">
<%
    settings = tmpl_context.settings
%>

${settings['company_name']} - ${settings['city']}, ${settings['state']} - Blog
</%def>


<div id="blog">
%for blog in blogs:
    <div class="blog-entry">
    <h3><a href="${blog.language and '/' + blog.language.code or ''}/blog/${blog.slug}">${blog.title}</a></h3>
    <div class="published-date">${blog.published.strftime("%-m/%-d/%Y")}</div>
    <%
        import re
        content = blog.abstract
        if not content:
            content = blog.content
        #strip image tags
        search = re.search('(<img.*?\/>|<img.*?\>)', content)

        if search:
            for group in search.groups():
                content = content.replace(group, '')
        content = content[:500]
    %>
    <p>${content | n} ...
    </p>
        <a class="small-button-style" href="${blog.language and '/' + blog.language.code or ''}/blog/${blog.slug}">More</a>

    </div>
%endfor

<div class="fr"><a href="/blog/atom1.xml" class="tooltip" alt="click to subscribe"><img class="tooltip" src="/images/feed.png"/></a></div>
</div>

