<%inherit file="para.core.templates.master"/>

<%def name="meta_description()">${blog.meta_description or tmpl_context.settings.get('meta_description', 'Blog')}</%def>
<%def name="meta_keywords()">${blog.meta_keywords or tmpl_context.settings.get('meta_keywords', 'blog, law, lawyer')}</%def>

<div id="blog">
    <div class="blog-entry">
    <h3>${blog.title}</h3>
    <div class="published-date">${blog.published.strftime("%-m/%-d/%Y")}</div>
    <p>${blog.content | n}</p>
    </div>
</div>