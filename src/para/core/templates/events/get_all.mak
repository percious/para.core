<%inherit file="para.core.templates.master"/>

<%def name="meta_description()">${tmpl_context.settings.get('events_meta_description', 'Events')}</%def>
<%def name="meta_keywords()">${tmpl_context.settings.get('events_meta_keywords', 'events, law, lawyer')}</%def>


<div id="event">
%for event in events:
    <div class="event-entry">
    <h3><a href="/events/${event.slug}">${event.name}</a></h3>
    <p>${event.datetime.strftime('%a %m-%d-%y %I:%M%p')}</p>
    <p>${event.description or '' | n}
    %if event.url:
        <a href="${event.url}">${event.url}</a>
    %endif
    </div>
%endfor
</div>