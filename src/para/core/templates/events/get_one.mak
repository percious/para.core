<%inherit file="para.core.templates.master"/>

<%def name="meta_description()">${event.meta_description or tmpl_context.settings.get('meta_description', event.name[:160])}</%def>
<%def name="meta_keywords()">${event.meta_keywords or tmpl_context.settings.get('meta_keywords', 'events, law, lawyer')}</%def>

<div id="event">
    <div class="event-entry">
    <h3>${event.name}</h3>
    <p>${event.datetime.strftime('%a %m-%d-%y %I:%M%p')}</p>
    <br/>
    <p>${event.description or '' | n}</p>
    <br/>
    %if event.url:
        <a href="${event.url}">${event.url}</a>
    %endif
    </div>

    %if event.location:
        <iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
        src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=${event.location.replace(' ', '+').lower()}&amp;aq=&amp;&amp;t=h&amp;ie=UTF8&amp;hq=&amp;hnear=${event.location.replace(' ', '+').lower()}}&amp;z=14&amp;output=embed"></iframe>
        <p>${event.location}</p>

    %endif
</div>