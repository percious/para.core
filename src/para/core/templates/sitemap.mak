<%inherit file="para.core.templates.master"/>

<h2>Home</h2>
<p>
<ul>
  <li><a href="${request.host}/">Home</a></li>
  <li><a href="${request.host}/about">About</a></li>
  <li><a href="${request.host}/blog">Blog</a></li>
  <li><a href="${request.host}/contact">Contact</a></li>
</ul>
</p>
   <h2>Pages</h2>
<p>
<ul>
   %for page in pages:
      <li><a href="${request.host}/${page.slug}">${page.title}</a></li>
   %endfor
</ul>
</p>
   <h2>Blogs</h2>
<p>
<ul>
   %for page in blogs:
      <li><a href="${request.host}/blog/${page.slug}">${page.title}</a></li>
   %endfor
</ul>
</p>

</p>
   <h2>Events</h2>
<p>
<ul>
   %for page in events:
      <li><a href="${request.host}/${page.slug}">${page.name}</a></li>
   %endfor
</ul>
</p>