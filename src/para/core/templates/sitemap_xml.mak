<?xml version="1.0" encoding="UTF-8"?>

<urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd" xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
   <url>
      <loc>http://${request.host}/</loc>
       <lastmod>${most_recent_modification}</lastmod>
       <changefreq>weekly</changefreq>
      <priority>1.0</priority>
   </url>

    %for language in languages:
   <url>
      <loc>http://${request.host}/${language}/</loc>
       <lastmod>${most_recent_modification}</lastmod>
       <changefreq>weekly</changefreq>
      <priority>1.0</priority>
   </url>
    %endfor

   <url>
      <loc>http://${request.host}/contact</loc>
      <lastmod>${about_page_modified}</lastmod>
      <changefreq>yearly</changefreq>
      <priority>0.9</priority>
   </url>

   %for page in pages:
   <url>
      %if page.language:
          <loc>http://${request.host}/${page.code}/${page.slug}</loc>
      %else:
          <loc>http://${request.host}/${page.slug}</loc>
      %endif
      <lastmod>${page.last_modified.strftime("%Y-%m-%d")}</lastmod>
      ##<changefreq>weekly</changefreq>
      <priority>0.8</priority>
   </url>
    %endfor

   <url>
      <loc>http://${request.host}/blog/</loc>
       %if blogs and blogs[0].last_modified :
      <lastmod>${blogs[0].last_modified.strftime("%Y-%m-%d")}</lastmod>
       %endif
      <changefreq>monthly</changefreq>
      <priority>0.8</priority>
   </url>

   %for language in languages:
   <url>
      <loc>http://${request.host}/${language}/blog/</loc>
       %if blogs and blogs[0].last_modified:
        <lastmod>${blogs[0].last_modified.strftime("%Y-%m-%d")}</lastmod>
       %endif
      <changefreq>weekly</changefreq>
      <priority>0.8</priority>
   </url>
    %endfor

    %for blog in blogs:
   <url>

      <loc>http://${request.host}/${blog.language and blog.code + '/'}blog/${blog.slug}</loc>
       %if blog.last_modified:
      <lastmod>${blog.last_modified.strftime("%Y-%m-%d")}</lastmod>
       %endif
      ## <changefreq>weekly</changefreq>
      <priority>0.6</priority>
   </url>
   %endfor

   %if events:
   <url>
      <loc>http://${request.host}/events/</loc>
       %if events and events[0].created:
      <lastmod>${events and events[0].created.strftime("%Y-%m-%d")}</lastmod>
       %endif
      <changefreq>monthly</changefreq>
      <priority>0.8</priority>
   </url>
    %endif

   %for language in languages:
   <url>
      <loc>http://${request.host}/${language}/events/</loc>
       %if events and events[0].created:
      <lastmod>${events and events[0].created.strftime("%Y-%m-%d")}</lastmod>
       %endif
      <changefreq>weekly</changefreq>
      <priority>0.8</priority>
   </url>
   %endfor

   %for event in events:
   <url>
      <loc>http://${request.host}/events/${event.slug}</loc>
       %if event.created:
      <lastmod>${event.created.strftime("%Y-%m-%d")}</lastmod>
       %endif
      ##<changefreq>weekly</changefreq>
      <priority>0.6</priority>
   </url>
   %endfor

</urlset>