<%inherit file="para.core.templates.master"/>

<%def name="meta_description()">${page.meta_description or tmpl_context.settings.get('meta_description', page.title[:160])}</%def>
<%def name="meta_keywords()">${page.meta_keywords or tmpl_context.settings.get('meta_keywords', 'law, lawyer')}</%def>
<%def name="meta_robots()">\
    %if page.hidden:
    <meta name="robots" content="noindex, nofollow" />
    %endif
</%def>


<%def name="title()">
<%
    settings = tmpl_context.settings
%>
${settings['company_name']} - ${settings['city']}, ${settings['state']} - ${page.title}
</%def>

${page.content | n}