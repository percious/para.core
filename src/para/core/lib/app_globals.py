# -*- coding: utf-8 -*-

"""The application's Globals object"""

__all__ = ['Globals']

from para.core.model import DBSession
from para.core.model.language import Language


class Globals(object):
    """Container for objects available throughout the life of the application.

    One instance of Globals is created during application initialization and
    is available during requests via the 'app_globals' variable.

    """

    @property
    def languages(self):
        if hasattr(self, '_languages'):
            return self._languages
        self._languages = [l.code for l in DBSession.query(Language.code).all()]
        return self._languages

    def __init__(self):
        pass