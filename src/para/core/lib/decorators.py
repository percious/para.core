from tg import request, config, redirect
from tg.decorators import before_validate
from webob.exc import HTTPMethodNotAllowed

@before_validate
def https(remainder, params):
    """
       Ensure that the decorated method is always called with https.
       Set para.core.https.override to ignore this decorator for testing purposes.
    """

    if config.get("para.https.override", False):
        return

    if request.environ.get('HTTP_X_FORWARDED_PROTO', '').lower() == 'https':
        return

    if request.method.upper() == 'GET':
        redirect('https' + request.url[len(request.scheme):])
    raise HTTPMethodNotAllowed(headers=dict(Allow='GET'))