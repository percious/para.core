import os

from tg import config
from tg.util import DottedFileNameFinder, DottedFileLocatorError
from tg.dottednames.mako_lookup import DottedTemplateLookup


class ParaDottedFileNameFinder(DottedFileNameFinder):
    def get_dotted_filename(self, template_name, template_extension='.html'):

        uri = template_name

        # allow the developer to provide a template package in their
        # ini file, which means you can have themes.
        local_app_name = config.get('para.template_package', config.get('para.package', 'para.core'))

        if uri.startswith('absolute:'):
            # we use an absolute url for the admin so that we can have
            # a unified interface for the back-end parts of para
            uri = uri[9:]
        elif uri.startswith('local:'):
            #if the template is "local" always point to para
            uri = 'para.core.' + uri[6:]
        elif uri.startswith('para.core.'):
            #otherwise, see if there is a template that matches in the local app
            local_uri = '.'.join([local_app_name,] + uri.split('.')[2:])
            fn = None
            try:
                fn = DottedFileNameFinder.get_dotted_filename(self, local_uri, template_extension='.mak')
            except DottedFileLocatorError:
                pass
            if fn and os.path.isfile(fn):
                return fn

        return DottedFileNameFinder.get_dotted_filename(self, uri, template_extension='.mak')


class ParaDottedTemplateLookup(DottedTemplateLookup):

    def adjust_uri(self, uri, relativeto):
        """Adjust the given uri relative to a filename.

        """

        result = config['tg.app_globals'].\
            dotted_filename_finder.get_dotted_filename(template_name=uri,
                                                       template_extension='.mak')

        if not uri in self.template_filenames_cache:
            # feed our filename cache if needed.
            self.template_filenames_cache[uri] = result

        return result