# -*- coding: utf-8 -*-

"""Caching mechanisms used for Para"""
from sqlalchemy.util._collections import KeyedTuple

from time import time
from para.core.model.language import Language
from para.core.model import DBSession


class TimedObjectCache(object):
    """Container for objects available throughout the life of the application.

    One instance of Globals is created during application initialization and
    is available during requests via the 'app_globals' variable.

    """

    def __init__(self, timeout=20):
        self._timeout = timeout
        self._last_access = {}
        self._the_data = {}

    def _do_get_objects(self, now, language):
        raise NotImplementedError

    def clear(self):
        self._the_data = {}
        self._last_access = {}

    def _data(self, lang):
        now = time()
        last_access = self._last_access.get(lang, None)

        if last_access and (now - last_access) < self._timeout:
            objs = self._the_data[lang]
            return objs

        language = DBSession.query(Language).filter(Language.code == lang).first()

        self._last_access[lang] = now
        objs = self._do_get_objects(now, language)

        #essentially, make the objects read only
        for obj in objs:
            if isinstance(obj, KeyedTuple):
                break
            if obj is None:
                break
            DBSession.expunge(obj)

        self._the_data[lang] = objs

        return objs

    def __getitem__(self, item):
        return self._data(item)
