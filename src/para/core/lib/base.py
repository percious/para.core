# -*- coding: utf-8 -*-

"""The base Controller API."""

import tg
from tg import TGController, tmpl_context
from tg import request
from tg.i18n import set_lang

import para.core.model as model
from para.core.model.page import Page
from para.core.lib.cache import TimedObjectCache

__all__ = ['BaseController', 'url']


def url(new_url):
    lang = tmpl_context.lang
    home = '/'

    if lang and lang != 'en_US':
        home = tg.url('/%s/' % lang)

    if new_url.startswith('/'):
        new_url = new_url[1:]

    return home + new_url

#xxx: make a lazy url for use in controller method decorators


class CachedFooter(TimedObjectCache):

    def _do_get_objects(self, now, language):
        if language is not None:
            footer = model.DBSession.query(Page.content).filter(Page.language == language).filter(Page.slug == 'footer').first()
            if footer:
                return [footer,]
        return [model.DBSession.query(Page.content).filter(Page.slug == 'footer').first(),]

cached_footer = CachedFooter(999)


class BaseController(TGController):
    """
    Base class for the controllers in the application.

    Your web application should have one of these. The root of
    your application is used to compute URLs used by your app.

    """

    def __call__(self, environ, start_response):
        """Invoke the Controller"""
        # TGController.__call__ dispatches to the Controller method
        # the request is routed to. This routing information is
        # available in environ['pylons.routes_dict']

        request.identity = request.environ.get('repoze.who.identity')
        tmpl_context.identity = request.identity
        tmpl_context.settings = model.settings

        lang = None

        path = environ['PATH_INFO'].strip(tg.url('/')).split('/')

        if path and path[0] in ['es']:
            lang = path[0]
            # had to dig in the tg code for this.
            # this was causing spanish not to be unset
            request.plain_languages = [lang]
        else:
            request.plain_languages = []

        set_lang(lang)


        tmpl_context.lang = lang
        tmpl_context.url = url

        self.lang = lang

        return TGController.__call__(self, environ, start_response)
